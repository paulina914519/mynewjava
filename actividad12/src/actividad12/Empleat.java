package actividad12;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Empleat implements Serializable{
	
	String Nom;
	String dni;
	int salari;
	public Empleat(String nom, String dni, int salari) {
		super();
		Nom = nom;
		this.dni = dni;
		this.salari = salari;
	}
	public Empleat() {
		super();
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public int getSalari() {
		return salari;
	}
	public void setSalari(int salari) {
		this.salari = salari;
	}
}
