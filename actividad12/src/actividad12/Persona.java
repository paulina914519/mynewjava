package actividad12;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Persona implements Serializable {
	
	String nom;
	int edat;
	
	public Persona(String nom, int edat) {
		super();
		this.nom = nom;
		this.edat = edat;
	}
	
	public Persona() {
		super();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}	
}
