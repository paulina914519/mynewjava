package actividad12;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


public class Client3Objecte {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		InetAddress desti = InetAddress.getLocalHost();
		int port = 12345;		
		Persona per = new Persona("Angeles", 25);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();		
		ObjectOutputStream outObjecte = new ObjectOutputStream(baos);
		outObjecte.writeObject(per);		
		byte[] missatge = baos.toByteArray();
		
		DatagramPacket enviament = new DatagramPacket
				(missatge, missatge.length, desti, port);		
		DatagramSocket socket = new DatagramSocket(45678);		
		socket.send(enviament);		
		System.out.println("Enviat: " + per.getNom() + "#" + per.getEdat());
		
		
		byte[] rebuts = new byte[1024];		
		DatagramPacket recepcio = new DatagramPacket(rebuts, rebuts.length);		
		socket.receive(recepcio);
		
		ByteArrayInputStream bais = new ByteArrayInputStream(rebuts);		
		ObjectInputStream in = new ObjectInputStream(bais);		
		Persona per2 = (Persona) in.readObject();		
		System.out.println("Rebut: " + per2.getNom() + "#" + per2.getEdat());
		
		outObjecte.close();
		in.close();
		socket.close(); 
		

	}

}
