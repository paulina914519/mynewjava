package actividad12;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client2Objecte {

	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {
		
		int numeroPort = 6000;		
		Socket client = new Socket("localhost", numeroPort);		
		
		ObjectOutputStream outObjecte = new ObjectOutputStream(client.getOutputStream());
		
		Empleat emp = new Empleat("Adrià","45789564M", 30000);
		
		outObjecte.writeObject(emp);
		
		System.out.println("Enviat: " + emp.getNom() + "#" + emp.getDni() + "#" + emp.getSalari());
		
		ObjectInputStream inObjecte = new ObjectInputStream(client.getInputStream());
		
		Empleat emp2 = (Empleat) inObjecte.readObject();
		
		System.out.println("Rebut: " + emp2.getNom() + "#" + emp2.getDni() + "#" + emp2.getSalari());
		
		outObjecte.close();
		inObjecte.close();
		client.close();

	}

}
