package actividad12;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.net.Socket;

public class Client1Objecte {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		int numeroPort = 6000;
		
		
		Socket client = new Socket("localhost", numeroPort);
		
		ObjectInputStream inObjecte = new ObjectInputStream(client.getInputStream());
		
		Persona per = (Persona) inObjecte.readObject();
		System.out.println("Rebut: " + per.getNom() + "#" + per.getEdat());
		
		per.setNom("Eduard");
		per.setEdat(35);
		
		ObjectOutputStream outObjecte = new ObjectOutputStream(client.getOutputStream());		
		
		outObjecte.writeObject(per);
		System.out.println("Enviat: " + per.getNom() + "#" + per.getEdat());		
		
		
		inObjecte.close();
		outObjecte.close();		
		client.close();
	}
}
