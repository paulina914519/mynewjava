package actividad12;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor2Objecte {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		int numeroPort = 6000;
		ServerSocket servidor = new ServerSocket(numeroPort);
		System.out.println("Esperant al client...");
		Socket client = servidor.accept();
		
		ObjectInputStream inObjecte = new ObjectInputStream(client.getInputStream());
		
		Empleat emp = (Empleat) inObjecte.readObject();
		
		System.out.println("Rebut: " + emp.getNom() + "#" + emp.getDni() + "#" + emp.getSalari());
		
		emp.setSalari(40000);
		
		ObjectOutputStream outObjecte = new ObjectOutputStream(client.getOutputStream());		
		
		outObjecte.writeObject(emp);
		System.out.println("Enviat: " + emp.getNom() + "#" + emp.getDni() + "#" + emp.getSalari());		
		
		inObjecte.close();
		outObjecte.close();		
		client.close();
		servidor.close();

	}

}
