package actividad12;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;


public class Servidor3Objecte {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		byte[] rebuts = new byte[1024];
		
		DatagramPacket recepcio = new DatagramPacket(rebuts, rebuts.length);		
		DatagramSocket socket = new DatagramSocket(12345);		
		socket.receive(recepcio);
		
		ByteArrayInputStream bais = new ByteArrayInputStream(rebuts);		
		ObjectInputStream in = new ObjectInputStream(bais);		
		Persona per = (Persona) in.readObject();		
		System.out.println("Rebut: " + per.getNom() + "#" + per.getEdat());
		
		per.setEdat(46);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();		
		ObjectOutputStream outObjecte = new ObjectOutputStream(baos);
		outObjecte.writeObject(per);
		
		byte[] missatge = baos.toByteArray();		
		DatagramPacket enviament = new DatagramPacket
				(missatge, missatge.length, recepcio.getAddress(), recepcio.getPort());		
		socket.send(enviament);		
		System.out.println("Enviat: " + per.getNom() + "#" + per.getEdat());
		
		in.close();
		outObjecte.close();
		socket.close(); 
	}
}
